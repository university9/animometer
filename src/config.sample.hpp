#ifndef CONFIG_HPP
#define CONFIG_HPP

#define NETWORK_SSID "ssid"
#define NETWORK_PASSWORD "password"

#define RESULTS_BASE_URL "https://url"
#define RESULTS_API_KEY "key"
#define RESULTS_ENVIRONMENT "environment"

#define THUMBS_UP_PIN 14
#define THUMBS_DOWN_PIN 15

#endif // CONFIG_HPP