#include "network.hpp"

Network::Network(const char* SSID, const char *passphrase) {
    WiFi.hostname("Animometro");
    WiFi.mode(WIFI_STA);
    this->multi.addAP(SSID, passphrase);
    Serial.println();
    Serial.println();
    Serial.print("Waiting for WiFi... ");

    this->connect();
}

void Network::reconnect() {
    if (WiFi.status() != WL_CONNECTED) {
        Serial.println("WiFi disconnected!");
        this->connect();
    }
};

void Network::connect() {
    Serial.println("Connecting WiFi");
    while(this->multi.run() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}