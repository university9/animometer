#ifndef RESULTS_HPP
#define RESULTS_HPP

#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

class Results {
    private:
    String baseUrl;
    String environment;
    String apiKey;

    public:
    Results(String baseUrl, String environment, String apiKey);
    void registerClick(bool happy);
};

#endif // RESULTS_HPP
