#ifndef NETWORK_HPP
#define NETWORK_HPP

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

class Network {
    ESP8266WiFiMulti multi;
    public:
        Network(const char* SSID, const char *passphrase = NULL);
        void connect();
        void reconnect();
};

#endif // NETWORK_HPP