#include "results.hpp"

Results::Results(String baseUrl, String environment, String apiKey) : baseUrl{baseUrl}, environment{environment}, apiKey{apiKey} {}

void Results::registerClick(bool happy) {

    //client->setFingerprint(fingerprint);
    WiFiClient client;
    HTTPClient https;

    Serial.print("[HTTPS] begin...\n");
    if (https.begin(client, this->baseUrl)) {  // HTTPS

      Serial.print("[HTTPS] POST...\n");

      https.addHeader("Content-Type", "application/x-www-form-urlencoded");
      String content = "key=" + this->apiKey + "&environment=" + this->environment + "&happy=" + (happy?"1":"0");
      Serial.println(content.c_str());
      // start connection and send HTTP header
      int httpCode = https.POST(content);
      https.writeToStream(&Serial);

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] POST... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTPS] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
}