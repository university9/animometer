#include <Arduino.h>
#include <JC_Button.h>
#include "config.hpp"
#include "network.hpp"
#include "results.hpp"

Network* network;
Results* results;

// Setup buttons
Button thumbsUpButton(THUMBS_UP_PIN, 100, true, true);
Button thumbsDownButton(THUMBS_DOWN_PIN, 100, true, true);

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  network = new Network(NETWORK_SSID, NETWORK_PASSWORD);
  results = new Results(RESULTS_BASE_URL, RESULTS_ENVIRONMENT, RESULTS_API_KEY);

  // Initialize buttons
  thumbsUpButton.begin();
  thumbsDownButton.begin();
}

void loop() {
  network->reconnect();
  // Test buttons
  thumbsUpButton.read();
  if (thumbsUpButton.wasPressed()) {
    Serial.println("Thumbs up");
    results->registerClick(true);
  }
  thumbsDownButton.read();
  if (thumbsDownButton.wasPressed()) {
    Serial.println("Thumbs down");
    results->registerClick(false);
  }
}