<?php
require('database.php');
// http://animometro.test/?key=itiCTounceAC&happy=0&environment=tests
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['key']) && $_POST['key'] == 'itiCTounceAC') {
        $happy = (isset($_POST['happy'])&&$_POST['happy'])=='1'?1:0;
        $environment = (isset($_POST['environment'])&&$_POST['environment']=='production')?$_POST['environment']:'tests';
        $database = new Database();
        $database->insertResult($environment, $happy);
        echo 'Done';
    } else {
        echo file_get_contents('php://input');;
        echo "Wrong key";
    }
}


?>