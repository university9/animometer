<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/secret.php");

class Database
{
    public function __construct() {
        $mysql = new mysqli(SERVERNAME, USERNAME, PASSWORD, DATABASE);

        if ($mysql->connect_error || !$mysql->select_db(DATABASE)) {
            $this->db = null;
        }
        else {
            $this->db = $mysql;
        }
    }

    /**
     * @param string $environment
     * @param bool $happy
     * @return bool success
     */
    public function insertResult(string $environment, bool $happy) {
        $query = $this->db->prepare("INSERT INTO ".$environment." (happy) VALUES (?)");
        $query->bind_param("i", $happy);

        if (!$query || !$query->execute()) return false;

        $query->close();
        return true;
    }

}